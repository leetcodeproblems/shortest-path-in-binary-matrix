https://leetcode.com/problems/shortest-path-in-binary-matrix/

Given an n x n binary matrix grid, return the length of the shortest clear path in the matrix. If there is no clear path, return -1.

A clear path in a binary matrix is a path from the top-left cell (i.e., (0, 0)) to the bottom-right cell (i.e., (n - 1, n - 1)) such that:

    All the visited cells of the path are 0.
    All the adjacent cells of the path are 8-directionally connected (i.e., they are different and they share an edge or a corner).

The length of a clear path is the number of visited cells of this path.

--> For the solution we will use BFS (Breadth-First Search)


**** BFS Template ****
// init
Int level = 0 ; //1 if the first step should be counted
Queue<Integer> q = new LinkedList<>();
Queue<Integer> p = new LinkedList<>();
Set<Integer> seen = new HashSet<>();

q.add(0);
seen.add(0);

while(!q.isEmpty()) {
    // 1- Pop front 
    int front = q.remove();

    // 2- Check if Goal 
    if( isGoal(front) ) return level ;

    // 3- Neighbors
    for(each neighbor) {
        If (check(neighbor) ) p.push(neighbor);
    }
    // 4- Check if level is over 
    if(q.size() == 0) {
        level ++ ;
        swap(p,q);
    }

}




