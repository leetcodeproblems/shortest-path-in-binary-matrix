//BFS
//using only one queue
class Solution {
        public int shortestPathBinaryMatrix(int[][] grid) {
        int n = grid.length;
        if(grid[0][0]!=0 || grid[n-1][n-1]!=0)
            return -1;
        int level = 1;
        Queue<int[]> queue = new LinkedList<>();
        boolean[][] seen = new boolean[n][n];
        
        queue.add(new int[]{0,0});
        seen[0][0]=true;
        while(!queue.isEmpty()){
            for(int i=queue.size()-1;i>=0;i--){
                 
                //front
                int[] front = queue.remove();
                int row = front[0], col =front[1];
                
                //check goal
                if(isGoal(row, col, grid))
                    return level;
                
                //Neighbors: all 8 directions
                int[] dx={0, 0, 1, -1, 1, 1, -1, -1};
                int[] dy={1, -1, 0, 0, 1, -1, 1, -1};
                
                for(int k=0;k<8;k++){
                    int toRow = row + dx[k];
                    int toCol = col + dy[k];
                    
                    if(check(toRow, toCol, grid, seen)){
                        queue.add(new int[]{toRow, toCol});
                        seen[toRow][toCol]=true;
                    }
                }
            }
            //level over
            level++;
        }
        
        return -1;
    }

    boolean check(int row, int col, int[][] grid, boolean[][] seen){
        return (row >=0) && (row < grid.length) && (col >= 0) &&  (col < grid[0].length) && (grid[row][col]==0) && (!seen[row][col]);
    }

    
    boolean isGoal(int row, int col, int[][] grid){
        return (row == grid.length -1) && (col == grid[0].length -1);
    }
}
